Using the quickstart from Faust documentation (https://faust.readthedocs.io/en/latest/playbooks/quickstart.html)
main.py contains the definition of a simple topic and a simple Faust App. This App can subscribe to a topic and consume it. The same App defined in main.py can produce messages to the same topic.

from a terminal run to start consuming topic
> faust -A main worker -l info

from a terminal run to write a message into the topic using the created app use
> faust -A main send @greet "Hello Faust"