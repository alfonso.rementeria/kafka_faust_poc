import faust

app = faust.App(
    'alfonsovalencia_app',
    broker='kafka://b-1.deu-msk-cluster.nc75k4.c2.kafka.eu-central-1.amazonaws.com:9092',
    value_serializer='raw',
)

# for topic parameters look into
# https://faust.readthedocs.io/en/latest/userguide/application.html#app-topic-create-a-topic-description
greetings_topic = app.topic('alfonsovalencia-poc.topic1')

@app.agent(greetings_topic)
async def greet(greetings):
    async for greeting in greetings:
        print(greeting)


# from terminal run to start consuming topic
# "faust -A main worker -l info"

# from terminal run to write a message into the topic using the created app use
# "faust -A main send @greet "Hello Faust""
